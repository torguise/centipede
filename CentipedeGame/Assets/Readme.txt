
Centipede - Coding Test
-----------------------

The goal of the test is to develop a simple game. 
There are some basic elements created for you to build on.

Key goals:
1) Develop a Centipede game. 
--- It should have at least one level of difficulty.
--- You should derive from BaseGame, and replace the script on the 'Game' object with your own.
2) If you have time, maintain and display a list of high-scores, which persists between runs of the game.
3) If you have remaining time, improve the game further in any way you think is most appropriate given your skills and the time remaining.

Please email your project back to jobs@pixeltoys.com at the end of the test.