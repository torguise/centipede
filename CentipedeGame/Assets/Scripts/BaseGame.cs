﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// --------------------------------------------------------------------------------------------------------
// 
// Base class for simple game game.
//
// Please derive from this class to implement your game.
// 
// --------------------------------------------------------------------------------------------------------

public class BaseGame : MonoSingleton<BaseGame>, IGame
{

	public GameObject resultsScreen;

	protected override void Awake()
	{
		base.Awake();
	}

	void Start() 
	{
	
	}

	void Update() 
	{
	
	}

	public virtual void HandleHit( GameObject object1 , GameObject object2 )
	{

	}

	public virtual void GameOver( int score )
	{
		resultsScreen.SetActive( true );
	}
}
