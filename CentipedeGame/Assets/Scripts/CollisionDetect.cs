﻿using UnityEngine;
using System.Collections;

public class CollisionDetect : MonoBehaviour 
{
	//Using collisions instead of triggers for simpler bounding for centipede and player
	private void OnCollisionEnter(Collision collision)
	{
		Debug.Log("**** collision detected: " + gameObject.name + " : " + collision.gameObject.name);
		if (BaseGame.Instance != null)
		{
			BaseGame.Instance.HandleHit(gameObject, collision.gameObject);
		}
	}

	//Allows for colliders to still be triggers (bullets)
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("**** trigger detected: " + gameObject.name + " : " + other.name);
		if (BaseGame.Instance != null) 
		{
			BaseGame.Instance.HandleHit( this.gameObject , other.gameObject );
		}
	}
}
