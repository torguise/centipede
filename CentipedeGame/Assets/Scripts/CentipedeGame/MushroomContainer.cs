﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomContainer : MonoBehaviour
{
	private const int COLUMNS = 9;
	private const int ROWS = 9;
	//unity units
	private const int GRID_WIDTH = 135;
	private const int GRID_HEIGHT = 150;

	[SerializeField] private GameObject mushroomPrefab;

	private List<GameObject> mushroomsPool = new List<GameObject>();
	private GameObject[] mushrooms = new GameObject[COLUMNS*ROWS];

	private int TotalCells => COLUMNS * ROWS;
	private float CellWidth => GRID_WIDTH / COLUMNS;
	private float CellHeight => GRID_HEIGHT / COLUMNS;

	public void CreateMushrooms(int numberOfMushrooms)
	{
		while (numberOfMushrooms > 0)
		{
			int column = Random.Range(0, COLUMNS - 1);
			int row = Random.Range(0, ROWS - 1);
			if(mushrooms[column+(COLUMNS * row)] == null)
			{
				GameObject mushroom = mushroomsPool.Count > 0 ? mushroomsPool[0] : CreateNewMushroom();
				mushroom.transform.localPosition = GetCellLocalPosition(column, row);
				mushroom.SetActive(true);
				numberOfMushrooms--;
			}
		}
	}

	public void ClearMushrooms()
	{
		for (int i = 0; i < mushrooms.Length; ++i)
		{
			GameObject mushroom = mushrooms[i];
			mushroomsPool.Add(mushroom);
			mushroom.SetActive(true);
			mushrooms[i] = null;
		}
	}

	private GameObject CreateNewMushroom()
	{
		return Instantiate(mushroomPrefab, transform);
	}
	
	private Vector3 GetCellLocalPosition(int column, int row)
	{
		//treat column/row as top left of each not center, set offsets for this tranform being center
		float columnOffset = (COLUMNS / 2) - 0.5f;
		float rowOffset = (ROWS / 2) - 0.5f;
		float xPos = (column - columnOffset) * CellWidth;
		float yPos = (row - rowOffset) * CellHeight;
		return new Vector3(xPos, yPos, 0);
	}
}
