﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags
{
	public const string PLAYER = "Player";
	public const string MUSHROOM = "mushroom";
	public const string CENTIPEDE = "centipede";
	public const string BULLET = "bullet";
	public const string BOUNDARY_LEFT_RIGHT = "boundary_l_r";
	public const string BOUNDARY_TOP_BOTTOM = "boundary_t_b";
}
