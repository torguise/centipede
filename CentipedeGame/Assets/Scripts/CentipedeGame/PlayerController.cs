﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private readonly Vector3 BULLET_OFFSET = new Vector3(0, 10, 0);

	private const string INPUT_HORIZONTAL = "Horizontal";
	private const string INPUT_VERTICAL = "Vertical";
	private const string INPUT_FIRE = "Fire1";

	[SerializeField] private Rigidbody bullet;
	[SerializeField] private float speed = 40;
	[SerializeField] private float bulletSpeed = 80;

	private Vector3 startingPosition;
	private bool inputBlocked;

	public void Reset()
	{
		SetInputBlocked(true);
		transform.position = startingPosition;
	}

	public void SetInputBlocked(bool blocked)
	{
		inputBlocked = blocked;
	}

	private void Awake()
	{
		startingPosition = transform.position;
	}

	private void Update()
    {
		if (!inputBlocked)
		{
			HandleMovement();
			HandleShooting();
		}
    }

	private void HandleMovement()
	{
		Vector3 direction = Vector3.zero;

		direction.x = Input.GetAxis(INPUT_HORIZONTAL);
		direction.y = Input.GetAxis(INPUT_VERTICAL);

		transform.position += (direction.normalized * speed) * Time.deltaTime;
	}

	private void HandleShooting()
	{
		if (bullet != null && !bullet.gameObject.activeSelf && Input.GetButton(INPUT_FIRE))
		{
			bullet.transform.position = transform.position + BULLET_OFFSET;
			bullet.velocity = new Vector3(0, 80, 0);
			bullet.gameObject.SetActive(true);
		}
	}
}
