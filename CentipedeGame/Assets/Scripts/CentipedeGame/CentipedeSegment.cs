﻿using System;
using UnityEngine;

public class CentipedeSegment : MonoBehaviour
{
	private const int BASE_SIZE = 10;		//uniform size
	private const int SINGLE_SIZE_SPEED_MULTIPLIER = 2;
	private const int SCORE_VALUE = 100;

	public static Action OnSegmentDestroyed;

	[SerializeField] private MeshRenderer meshRenderer;
	[SerializeField] private Material HeadMaterial;
	[SerializeField] private Material BodyMaterial;

	public CentipedeSegment Previous { get; set; }
	public CentipedeSegment Next { get; set; }
	
	private float verticalDistanceTravelled = 0;
	private Quaternion initialRotation;

	private bool directionFlipped = false;
	private bool moveVertically = false;

	public bool IsHead
	{
		get
		{
			return Previous == null || !Previous.gameObject.activeSelf;
		}
	}

	public void Awake()
	{
		initialRotation = transform.rotation;
	}

	public void Initialise(Vector3 initialSpawnPosition)
	{
		directionFlipped = false;
		gameObject.SetActive(true);
		Vector3 positionOffset = new Vector3(BASE_SIZE * transform.localScale.x, 0, 0);

		if (Previous == null)
		{
			transform.position = initialSpawnPosition;
		}
		else
		{
			//offset from last segment, including scale, right to left
			transform.position = Previous.transform.position - positionOffset;
		}

		transform.rotation = initialRotation;
		UpdateMaterial();
	}
	
	public void Move(float speed)
	{
		if (gameObject.activeSelf)
		{
			if (IsHead)
			{
				speed = Next == null || !Next.gameObject.activeSelf ? speed * SINGLE_SIZE_SPEED_MULTIPLIER : speed;
			}

			//-transform.right accounts for the facing of the graphic
			Vector3 moveVector = (-transform.right * speed) * Time.deltaTime;
			
			if (moveVertically)
			{
				verticalDistanceTravelled += Mathf.Abs(moveVector.y);
				//add the extra to the new travel direction
				float difference = verticalDistanceTravelled - BASE_SIZE * transform.localScale.y;
				//rotate object
				if (difference >= 0)
				{
					//bound to distance and move in new direction by difference for no loss of speed
					moveVector.y -= difference;
					moveVector.x = difference;
					ChangeDirection();
					return;
				}
			}

			transform.position += moveVector;
		}
	}

	public int DestroySegment()
	{
		gameObject.SetActive(false);

		if (Next && Next.gameObject.activeSelf)
		{
			Next.UpdateMaterial();
		}
		OnSegmentDestroyed();
		return SCORE_VALUE;
	}

	public void UpdateMaterial()
	{
		meshRenderer.material = IsHead ? HeadMaterial : BodyMaterial;
	}
	
	//TODO handle fliping vertical direction, probably a flag: bool vertical, Also need to handle changing direction if hitting top of mushroom
	public void ChangeDirection()
	{
		float angle = directionFlipped ? 90 : -90;
		transform.Rotate(Vector3.forward, angle, Space.World);

		//if moving vertically when changing direction, new direction will be flipped direction
		if (moveVertically)
		{
			directionFlipped = !directionFlipped;
		}
		moveVertically = !moveVertically;
		verticalDistanceTravelled = 0;      //reset vertical distance tracker
	}

	//TODO: move collision handling into here?
	//public void HandleCollision()
	//{
		//TODO: add adjustment for boundary collision
		//float maxCollisionDistance = other.width/2 + this.width/2
		//float xAxisCollisionOverlap = maxCollisionDistance - Mathf.abs(other.transform.x - transform.x) //need to account for 1/2 width of each
		//float xAdjust = directionFlipped ? -xAxisCollisionOverlap : xAxisCollisionOverlap;
		//this.transform.localposition += newVector3 (xAdjust, -xAxisDistance ,0);
		//verticalDistanceTravelled += xAxisDistance;
	//}
}
