﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Centipede : MonoBehaviour
{
	public const int DEFAULT_SIZE = 5;
	private const int DEFAULT_SPEED = 30;

	[SerializeField] private GameObject segmentPrefab;
	[SerializeField] private Transform initialSpawnPoint;

	private CentipedeSegment firstSegment;
	private CentipedeSegment lastSegment;

	private float speedModifier = 1;

	private void Awake()
	{
		InitialiseSegments();
	}

	private void OnEnable()
	{
		CentipedeSegment.OnSegmentDestroyed += OnSegmentDestroyed;
	}

	private void OnDisable()
	{
		CentipedeSegment.OnSegmentDestroyed -= OnSegmentDestroyed;
	}

	//Load existing segments under this object
	private void InitialiseSegments()
	{
		CentipedeSegment[] segments = GetComponentsInChildren<CentipedeSegment>(true);
		if (segments.Length > 1)
		{
			for (int i = 1; i < segments.Length; ++i)
			{
				CentipedeSegment current = segments[i];
				CentipedeSegment previous = segments[i - 1];

				current.Previous = previous;
				previous.Next = current;
			}
		}
		if (segments.Length > 0)
		{
			firstSegment = segments[0];
			lastSegment = segments[segments.Length - 1];
		}
	}

	public void SpawnNew(int size)
	{
		CentipedeSegment previousSegment = null;
		CentipedeSegment currentSegment = firstSegment;
		for(int i = 0; i < size; ++i)
		{
			if(currentSegment == null)
			{
				//create new segment at end of collection
				currentSegment = CreateSegment(previousSegment);
			}
			currentSegment.Initialise(initialSpawnPoint.position);

			previousSegment = currentSegment;
			currentSegment = currentSegment.Next;
		}
	}

	//creates a new segement at the end of the centipede
	private CentipedeSegment CreateSegment(CentipedeSegment previous)
	{
		CentipedeSegment newSegment = null;
		if (segmentPrefab != null)
		{
			GameObject go = Instantiate(segmentPrefab, transform);
			newSegment = go.GetComponent<CentipedeSegment>();
			if (newSegment == null)
			{
				Debug.LogError("Failed to create new segment: segmentPrefab does not contain component CentipedeSegment");
			}
			else
			{
				newSegment.Previous = previous;
				if(previous != null)
				{
					previous.Next = newSegment;
				}

				if(firstSegment == null)
				{
					firstSegment = newSegment.Previous;
				}
				//new segment is always the new last in collection
				lastSegment = newSegment;
			}
		}
		else
		{
			Debug.LogError("Failed to create new segment: segmentPrefab not set");
		}
		return newSegment;
	}

	private void Update()
	{
		CentipedeSegment current = lastSegment;
		while(current != null)
		{
			current.Move(DEFAULT_SPEED * speedModifier);
			current = current.Previous;
		}
	}

	private void OnSegmentDestroyed()
	{
		bool completelyDestroyed = true;
		CentipedeSegment current = firstSegment;
		while(current != null)
		{
			if (current.gameObject.activeSelf)
			{
				completelyDestroyed = false;
				break;
			}
			current = current.Next;
		}

		if (completelyDestroyed)
		{
			speedModifier += 0.1f;		//increases difficulty by increasing speed
			SpawnNew(DEFAULT_SIZE);
		}
	}
}
