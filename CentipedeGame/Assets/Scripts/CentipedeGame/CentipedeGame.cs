﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CentipedeGame : BaseGame
{
	[SerializeField] private Centipede centipede;
	[SerializeField] private PlayerController player;
	[SerializeField] private MushroomContainer mushroomContainer;
	[SerializeField] private Text scoreLabel;
	[SerializeField] private GameObject playButton;

	private int score = 0;
	private ResultsScreen resultsScreenController;

	protected override void Awake()
	{
		base.Awake();
		resultsScreenController = resultsScreen.GetComponent<ResultsScreen>();
		Random.InitState(System.DateTime.Now.Millisecond);
	}

	public void Start()
	{
		ResetGame();
	}

	public void OnPlayPressed()
	{
		//UI
		playButton.SetActive(false);
		resultsScreen.SetActive(false);
		scoreLabel.gameObject.SetActive(true);
		//Entities
		player.gameObject.SetActive(true);
		player.SetInputBlocked(false);
		centipede.gameObject.SetActive(true);
		centipede.SpawnNew(Centipede.DEFAULT_SIZE);
		//spawn mushrooms
		mushroomContainer.CreateMushrooms(10);
	}

	public override void HandleHit(GameObject object1, GameObject object2)
	{
		if (object1.tag.CompareTo(Tags.CENTIPEDE) == 0)
		{
			CentipedeSegment centipedeSegment = object1.GetComponent<CentipedeSegment>();
			switch (object2.tag)
			{
				//TODO: handle flipping vertical direction on top/bottom boundary
				case Tags.BOUNDARY_LEFT_RIGHT:
				case Tags.MUSHROOM:
					centipedeSegment.ChangeDirection();
					break;
				case Tags.BULLET:
					IncreaseScore(centipedeSegment.DestroySegment());
					break;
				case Tags.PLAYER:
					player.gameObject.SetActive(false);
					GameOver(score);
					break;
			}
		}

		if (object1.tag.CompareTo(Tags.BULLET) == 0)
		{
			if (object2.tag.CompareTo(Tags.PLAYER) != 0)
			{
				object1.SetActive(false);
			}
		}
	}

	public override void GameOver(int score)
	{
		resultsScreenController.AddScore(score);
		base.GameOver(score);
		ResetGame();
	}

	private void IncreaseScore(int increaseValue)
	{
		score += increaseValue;
		scoreLabel.text = score.ToString();
	}

	private void ResetGame()
	{
		IncreaseScore(-score);
		mushroomContainer.ClearMushrooms();
		player.Reset();
		centipede.gameObject.SetActive(false);
		scoreLabel.gameObject.SetActive(false);
		playButton.SetActive(true);
	}
}