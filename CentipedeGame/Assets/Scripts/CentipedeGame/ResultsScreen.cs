﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{
	[SerializeField] Text resultsScoreLabel;
    
	public void AddScore(int score)
	{
		resultsScoreLabel.text = score.ToString();
	}
}
